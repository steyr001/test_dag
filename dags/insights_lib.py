from datetime import datetime, timedelta
import logging
import sys
import time
import random 

def whoami():
    return sys._getframe(1).f_code.co_name

def preprocess_britbox_1h_data(ds, **kwargs):
    logging.info(kwargs)
    logging.info(whoami())
    time.sleep(random.randint(1, 100))
    return whoami()+' ready'

def preprocess_firstlookmedia_1h_data(ds, **kwargs):
    logging.info(kwargs)
    logging.info(whoami())
    time.sleep(random.randint(1, 100))
    return whoami()+' ready'

def preprocess_nbc_telemundo_1h_data(ds, **kwargs):
    logging.info(kwargs)
    logging.info(whoami())
    time.sleep(random.randint(1, 100))
    return whoami()+' ready'

def collect_1h_data(ds, **kwargs):
    logging.info(kwargs)
    logging.info(whoami())
    print(ds)
    time.sleep(random.randint(1, 100))
    return whoami()+' ready'

def calculate_1h_insights(ds, **kwargs):
    logging.info(kwargs)
    logging.info(whoami())
    print(ds)
    time.sleep(random.randint(1, 100))
    return whoami()+' ready'

def preprocess_britbox_15m_data(ds, **kwargs):
    logging.info(kwargs)
    logging.info(whoami())
    time.sleep(random.randint(1, 100))
    return whoami()+' ready'

def preprocess_firstlookmedia_15m_data(ds, **kwargs):
    logging.info(kwargs)
    logging.info(whoami())
    time.sleep(random.randint(1, 100))
    return whoami()+' ready'

def preprocess_nbc_telemundo_15m_data(ds, **kwargs):
    logging.info(kwargs)
    logging.info(whoami())
    time.sleep(random.randint(1, 100))
    return whoami()+' ready'

def collect_15m_data(ds, **kwargs):
    logging.info(kwargs)
    logging.info(whoami())
    print(ds)
    time.sleep(random.randint(1, 100))
    return whoami()+' ready'

def calculate_15m_insights(ds, **kwargs):
    logging.info(kwargs)
    logging.info(whoami())
    print(ds)
    time.sleep(random.randint(1, 100))
    return whoami()+' ready'

def preprocess_britbox_1d_data(ds, **kwargs):
    logging.info(kwargs)
    logging.info(whoami())
    time.sleep(random.randint(1, 100))
    return whoami()+' ready'

def preprocess_firstlookmedia_1d_data(ds, **kwargs):
    logging.info(kwargs)
    logging.info(whoami())
    time.sleep(random.randint(1, 100))
    return whoami()+' ready'

def preprocess_nbc_telemundo_1d_data(ds, **kwargs):
    logging.info(kwargs)
    logging.info(whoami())
    time.sleep(random.randint(1, 100))
    return whoami()+' ready'

def collect_1d_data(ds, **kwargs):
    logging.info(kwargs)
    logging.info(whoami())
    print(ds)
    time.sleep(random.randint(1, 100))
    return whoami()+' ready'

def calculate_1d_insights(ds, **kwargs):
    logging.info(kwargs)
    logging.info(whoami())
    print(ds)
    time.sleep(random.randint(1, 100))
    return whoami()+' ready'

def save2druid(ds, **kwargs):
    logging.info(kwargs)
    logging.info(whoami())
    print(ds)
    time.sleep(random.randint(1, 100))
    return whoami()+' ready'

def send_notification_messages(ds, **kwargs):
    logging.info(kwargs)
    logging.info(whoami())
    print(ds)
    time.sleep(random.randint(1, 100))
    return whoami()+' ready'
