from airflow import DAG
from airflow.operators.bash_operator import BashOperator
from airflow.operators.python_operator import PythonOperator
from datetime import datetime, timedelta
import logging
import sys
import time
import random 

from insights_lib import preprocess_firstlookmedia_15m_data, preprocess_britbox_15m_data, preprocess_nbc_telemundo_15m_data, collect_15m_data, calculate_15m_insights, save2druid, send_notification_messages

default_args = {
    "owner": "pickaxe",
    "depends_on_past": False,
    "start_date": datetime(2018, 11, 27, 11),
    "email": ["piotr@pickaxe.ai"],
    "email_on_failure": False,
    "email_on_retry": False,
    "retries": 1,
    "retry_delay": timedelta(minutes=5),
    # 'queue': 'bash_queue',
    # 'pool': 'backfill',
    # 'priority_weight': 10,
    # 'end_date': datetime(2016, 1, 1),
}

dag = DAG("15_minutes_insights", default_args=default_args, schedule_interval=timedelta(minutes=15))

preprocess_flm = PythonOperator(
                            task_id='preprocess_firstlookmedia_15m_data',
                            provide_context=True,
                            python_callable=preprocess_firstlookmedia_15m_data,
                            dag=dag)

preprocess_bbm = PythonOperator(
                            task_id='preprocess_britbox_15m_data',
                            provide_context=True,
                            python_callable=preprocess_britbox_15m_data,
                            dag=dag)

preprocess_nbc = PythonOperator(
                            task_id='preprocess_nbc_telemundo_15m_data',
                            provide_context=True,
                            python_callable=preprocess_nbc_telemundo_15m_data,
                            dag=dag)

collect_15m_data = PythonOperator(
                            task_id='collect_15m_data',
                            provide_context=True,
                            python_callable=collect_15m_data,
                            dag=dag)

calc_15m_insights = PythonOperator(
                            task_id='calculate_15m_insights',
                            provide_context=True,
                            python_callable=calculate_15m_insights,
                            dag=dag)

druid_save = PythonOperator(
                            task_id='save2druid',
                            provide_context=True,
                            python_callable=save2druid,
                            dag=dag)

notification = PythonOperator(
                            task_id='send_notification_messages',
                            provide_context=True,
                            python_callable=send_notification_messages,
                            dag=dag)

collect_15m_data.set_upstream(preprocess_flm)
collect_15m_data.set_upstream(preprocess_bbm)
collect_15m_data.set_upstream(preprocess_nbc)

calc_15m_insights.set_upstream(collect_15m_data)

druid_save.set_upstream(calc_15m_insights)
notification.set_upstream(calc_15m_insights)

